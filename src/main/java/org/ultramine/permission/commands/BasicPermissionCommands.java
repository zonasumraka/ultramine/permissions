package org.ultramine.permission.commands;

import org.ultramine.commands.Command;
import org.ultramine.commands.CommandContext;
import org.ultramine.core.service.InjectService;
import org.ultramine.permission.IPermissionManager;

public class BasicPermissionCommands
{
	@InjectService
	private static IPermissionManager perms;

	@Command(
			name = "pconfig",
			group = "permissions",
			aliases = {"permissions", "pcfg"},
			permissions = {"permissions.admin"},
			syntax = {"[save reload]"}
	)
	public static void pcofnig(CommandContext ctx)
	{
		if (ctx.actionIs("save"))
		{
			perms.save();
			ctx.notifyAdmins("command.pconfig.success.save");
		}
		else
		{
			perms.reload();
			ctx.notifyAdmins("command.pconfig.success.reload");
		}
	}

	@Command(
			name = "puser",
			group = "permissions",
			permissions = {"permissions.admin.user"},
			syntax = {
					"<player> [group setgroup] <group>",
					"<player> [add remove] <permission>...",
					"<player> [meta] <pmeta> <%value>",
					"<world> <player> [add remove] <permission>...",
					"<world> <player> [meta] <pmeta> <%value>"
			}
	)
	public static void puser(CommandContext ctx)
	{
		String player = ctx.get("player").asString();
		String world = ctx.contains("world")
				? ctx.get("world").asWorld().getWorldInfo().getWorldName()
				: IPermissionManager.GLOBAL_WORLD;

		ctx.checkSenderPermissionInWorld(world, "permissions.admin.world");

		if (ctx.actionIs("group") || ctx.actionIs("setgroup"))
		{
			perms.setUserGroup(player, ctx.get("group").asString());
			ctx.sendMessage("command.puser.success.group", player, ctx.get("group").asString());
		}
		else if (ctx.actionIs("add"))
		{
			for (CommandContext.Argument arg : ctx.get("permission").asArray())
			{
				perms.add(world, player, arg.asString());
				ctx.sendMessage("command.puser.success.add", arg.asString(), player, world);
			}
		}
		else if (ctx.actionIs("remove"))
		{
			for (CommandContext.Argument arg : ctx.get("permission").asArray())
			{
				perms.remove(world, player, arg.asString());
				ctx.sendMessage("command.puser.success.remove", arg.asString(), player, world);
			}
		}
		else
		{
			String key = ctx.get("pmeta").asString();
			String value = ctx.get("value").asString();

			perms.setMeta(world, player, key, value);
			ctx.sendMessage("command.puser.success.meta", key, value, player, world);
		}

		perms.save();
	}


	@Command(
			name = "pmixin",
			group = "permissions",
			permissions = {"permissions.admin.mixin"},
			syntax = {
					"<mixin> [add remove] <permission>...",
					"<mixin> [meta] <pmeta> <%value>"
			}
	)
	public static void pmixin(CommandContext ctx)
	{
		String mixin = ctx.get("mixin").asString();

		if (ctx.actionIs("add"))
		{
			for (CommandContext.Argument arg : ctx.get("permission").asArray())
			{
				perms.addToMixin(mixin, arg.asString());
				ctx.sendMessage("command.pmixin.success.add", arg.asString(), mixin);
			}
		}
		else if (ctx.actionIs("remove"))
		{
			for (CommandContext.Argument arg : ctx.get("permission").asArray())
			{
				perms.removeFromMixin(mixin, arg.asString());
				ctx.sendMessage("command.pmixin.success.remove", arg.asString(), mixin);
			}
		}
		else
		{
			String key = ctx.get("key").asString();
			String value = ctx.get("value").asString();
			perms.setMixinMeta(mixin, key, value);
			ctx.sendMessage("command.pmixin.success.meta", key, value, mixin);
		}

		perms.save();
	}
	
	@Command(
			name = "pgroup",
			group = "permissions",
			permissions = {"permissions.admin.group"},
			syntax = {
					"<group> [parent setparent] <group%parent>",
					"<group> [add remove] <permission>...",
					"<group> [meta] <pmeta> <%value>",
					"<group> <world> [add remove] <permission>...",
					"<group> <world> [meta] <pmeta> <%value>"
			}
	)
	public static void pgroup(CommandContext ctx)
	{
		String group = ctx.get("group").asString();
		String world = ctx.contains("world")
				? ctx.getServer().getMultiWorld().getNameByID(ctx.get("world").asWorld().provider.dimensionId)
				: IPermissionManager.GLOBAL_WORLD;

		if (ctx.actionIs("parent") || ctx.actionIs("setparent"))
		{
			perms.setUserGroup(group, ctx.get("parent").asString());
			ctx.sendMessage("command.pgroup.success.parent", ctx.get("parent").asString(), group);
		}
		if (ctx.actionIs("add"))
		{
			for (CommandContext.Argument arg : ctx.get("permission").asArray())
			{
				perms.addToGroup(group, world, arg.asString());
				ctx.sendMessage("command.pgroup.success.add", arg.asString(), group);
			}
		}
		else if (ctx.actionIs("remove"))
		{
			for (CommandContext.Argument arg : ctx.get("permission").asArray())
			{
				perms.removeFromGroup(group, world, arg.asString());
				ctx.sendMessage("command.pgroup.success.remove", arg.asString(), group);
			}
		}
		else
		{
			String key = ctx.get("key").asString();
			String value = ctx.get("value").asString();
			perms.setGroupMeta(group, world, key, value);
			ctx.sendMessage("command.pgroup.success.meta", key, value, group);
		}

		perms.save();
	}
}
